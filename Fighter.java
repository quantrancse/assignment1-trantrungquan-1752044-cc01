class Fighter {
    private int mBaseHp;
    private int mWp;

    public double getCombatScore() {
        return 0.0;
    }

    Fighter(int BaseHp, int Wp) {
        mBaseHp = BaseHp;
        mWp = Wp;
    }

    public double getBaseHp() {
        return mBaseHp;
    }

    public void setBaseHp(int baseHp) {
        mBaseHp = baseHp;
    }

    public int getWp() {
        return mWp;
    }

    public void setWp(int wp) {
        mWp = wp;
    }
}
