class Knight extends Fighter {
    Knight(int baseHp, int wp) {
        super(baseHp, wp);
    }

    public double getCombatScore() {
        if (Utility.Ground == 999) {
            double result = 1.0;
            double f0 = 0.0;
            double f1 = 1.0;
            if (super.getBaseHp() == 0)
                return 1;
            while (result < super.getBaseHp() * 2) {
                f0 = f1;
                f1 = result;
                result = f0 + f1;
            }
            return result;
        } 
        else if (Utility.isSquare(Utility.Ground) == true) 
            return super.getBaseHp() * 2;
        else if (super.getWp() == 1)
            return super.getBaseHp();
        else 
            return super.getBaseHp() / 10;
    }
}
