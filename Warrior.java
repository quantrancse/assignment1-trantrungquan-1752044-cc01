class Warrior extends Fighter {
    Warrior(int baseHp, int wp) {
        super(baseHp, wp);
    }

    public double getCombatScore() {
        if (Utility.isPrime(Utility.Ground) == true) 
            return super.getBaseHp() * 2;
        else if (super.getWp() == 1)
            return super.getBaseHp();
        else 
            return super.getBaseHp() / 10;
    }
}
